# hackandbot
This project contains a Discord Bot for the [hackandcode Discord-Server](https://discord.gg/eaysm4PsSt)!

## Prepare your Environment

To be able to run the Bot you first need to fill out the `cogs/settings.py`.
It contains all needed values to run this Bot with all it's features (some of them are optional).

The `settings.py` looks like this:
```python
bot_token = ''

# Channel IDs for logs
log_channel = 
warn_channel = 
report_channel = 
mute_channel = 

# voice section
voice_channel_id =
use_voice_role = True
use_user_voice = True
voice_category_name = ''
voice_role_name = ''

# administration section
verify_role_name = ''
mute_role_name = ''
admin_role_name = ''
mod_role_name = ''
embed_thumbnail = ''

# social links
youtube_link = ''
discord_link = ''
github_link = ''
insta_link = ''
twitter_link = ''
tiktok_link = ''
twitch_link = ''
```

## Run the Bot without Docker

To run the Bot without Docker you first need to install the required Python modules by typing:
```bash
pip install --no-cache-dir -r requirements.txt
```

After the modules are installed you are able to execute the Bot by running:
```bash
python3 main.py
```

## Run the Bot with Docker

If you want the Bot to run inside your Docker Environment you can simply execute the `docker-compose.yml` and the Image will be built.

