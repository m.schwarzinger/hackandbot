from discord.ext import commands
from cogs.settings import youtube_link, insta_link, discord_link, github_link, tiktok_link, twitter_link, twitch_link

class Social(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.slash_command(name="youtube", description="Show YouTube Link")
    async def yt(self, ctx):
        if youtube_link != '':
            await ctx.send(youtube_link)

    @commands.slash_command(name="invite", description="Show Invite Link")
    async def invite(self, ctx):
        if discord_link != '':
            await ctx.send(discord_link)

    @commands.slash_command(name="github", description="Show GitHub Link")
    async def github(self, ctx):
        if github_link != '':
            await ctx.send(github_link)

    @commands.slash_command(name="instagram", description="Show Instagram Link")
    async def insta(self, ctx):
        if insta_link != '':
            await ctx.send(insta_link)

    @commands.slash_command(name="tiktok", description="Show TikTok Link")
    async def tiktok(self, ctx):
        if tiktok_link != '':
            await ctx.send(tiktok_link)

    @commands.slash_command(name="twitter", description="Show Twitter Link")
    async def twitter(self, ctx):
        if tiktok_link != '':
            await ctx.send(twitter_link)

    @commands.slash_command(name="twitch", description="Show Twitch Link")
    async def twitch(self, ctx):
        if twitch_link != '':
            await ctx.send(twitch_link)



def setup(client):
    client.add_cog(Social(client))
    print('Social cog loaded')
